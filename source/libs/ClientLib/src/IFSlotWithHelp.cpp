///******************************************************************************
/// \File IFSlotWithHelp.cpp
///
/// \Desc
///
/// \Author kyuubi09 on 6/21/2023.
///
/// \Copyright Copyright © 2023 SRO_DevKit.
///
///******************************************************************************

#include "IFSlotWithHelp.h"

void CIFSlotWithHelp::SetSlotData(CSOItem *pItemSocket) {
    reinterpret_cast<void (__thiscall *)(CIFSlotWithHelp *, CSOItem *)>(0x006871d0)(this, pItemSocket);
}
